const SECTION_ONE_SUBTITLE = "هر روز یاد بگیر";
const SECTION_ONE_DESCRIPTION = "روزانه از ویدیو ها و پادکست های آموزش رایگان استفاده کن کسب و کار خودت را رشد بده.";
const SECTION_TWO_SUBTITLE = "هر روز انرژی بگیر";
const SECTION_TWO_DESCRIPTION = "روزانه از ویدیو ها و پادکست های انگیزشی رایگان استفاده کن و برای کار کردن انرژی بگیر .";
const SECTION_TREE_SUBTITLE = "هر روز یاد بگیر";
const SECTION_TREE_DESCRIPTION =
  "میتونی با خریداری کتاب های صوتی برتر جهان در حوزه کسب و کار و موفقیت (به زبان فارسی) با صدای گویندگان حرفه ای به اونا تو اپلیکیشن اندیشه روشن دسترسی داشته باشی وهر وقت دلت خواست به کتاب گوش بدی .";
const SECTION_FOUR_SUBTITLE = "هر روز سمینار برو ";
const SECTION_FOUR_DESCRIPTION = " میتونی به فیلم سمینار برترین اساتید ایران و جهان دسترسی داشته باشی و تو اپلیکیشن اندیشه روشن هر وقت خواستی توی سمینار ها شرکت کنی.";

const SECTION_FIVE_SUBTITLE = "هر روز رشد کن";
const SECTION_FIVE_DESCRIPTION = " میتونی دوره های تخصصی در حوزه های مختلف کسب و کار فردی رو از اپلیکشن بخری و به صورت صوتی و تصویری ازشون استفاده کنی.";

const SECTION_SIX_SUBTITLE = "هر روز تجربه کن";
const SECTION_SIX_DESCRIPTION = "میتونی از ویدیو های رایگان آنالیز مشاغل استفاده کنی و شغل انتخاب کنی یا اطلاعاتت را در زمینه مشاغل زیاد کنی .";

// ======================= about us
const ABOUT_US =
  "(مجموعه آموزشی مجازی اندیشه روشن) مجموعه آموزشی اندیشه روشن در آبان ماه سال ۱۳۹۸ با هدف ارتقای دانش و مهارت های کسب و کار، فعالیت خود را در بستر فضای مجازیو شبکه اینترنت آغاز نمود و رسالت خود را ارتقای هر چه بیشتر دانش عمومی برای صاحبان کسب و کار و کمک به کارآفرینی جوانان جویای کار قرار داده است. امید است روزیهر ایرانی صاحب کسب و کار شخصی خود باشد.";
// ===================== ContactUS
const CONTACT_US_P1 = "شما همراهان گرامی می توانید سوالات، نظرات و هر گونه انتقاد و پیشنهادات خود را از راه های ارتباطی زیر با ما در میان بگذارید .";
const CONTACT_US_P2 = "نظرات و پیشنهادات شما راهنمای ما برای ادامه راه خواهد بود";
const PHONE_NUMBER = "09394018034";
const ADDRESS = "رشت گلسار خیابان استاد معین نبش کوچه ۲۹ پلاک ۱۲";
const TELEGRAM_LOCATION = "https://t.me/Andisheroshant";
const string = {
  SECTION_ONE_DESCRIPTION,
  SECTION_ONE_SUBTITLE,
  SECTION_TWO_SUBTITLE,
  SECTION_TWO_DESCRIPTION,
  SECTION_TREE_SUBTITLE,
  SECTION_TREE_DESCRIPTION,
  SECTION_FOUR_SUBTITLE,
  SECTION_FOUR_DESCRIPTION,
  SECTION_FIVE_SUBTITLE,
  SECTION_FIVE_DESCRIPTION,
  SECTION_SIX_SUBTITLE,
  SECTION_SIX_DESCRIPTION,
  ABOUT_US,
  CONTACT_US_P1,
  CONTACT_US_P2,
  PHONE_NUMBER,
  ADDRESS,
  TELEGRAM_LOCATION
};
export default string;
