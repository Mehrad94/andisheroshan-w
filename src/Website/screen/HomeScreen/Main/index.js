import React, { useRef } from "react";
import "./index.scss";
import MainSection from "../../../components/MainSection";
import consts from "../../../util/consts";
const Main = () => {
  const sectionsmap = consts.mainSections.map((section, index) => {
    return <MainSection {...section} key={index} />;
  });
  return (
    <main className="main-wrapper">
      <div className="web-container">{sectionsmap}</div>
    </main>
  );
};

export default Main;
