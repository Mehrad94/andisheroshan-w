import React from "react";
import Header from "../../components/Header";
import MainScreen from "./Main";
import Footer from "../../components/Footer";
import MobileApp from "../../components/MobileApp";
import SliderScrollFull from "../../components/SliderScrollFull";

const HomeScreen = () => {
  return (
    <React.Fragment>
      <Header main={true} />
      <MainScreen />
      <Footer />
    </React.Fragment>
  );
};

export default HomeScreen;
