import React, { Fragment } from "react";
import "./index.scss";
import tlIcon from "../../assets/images/icon/telegram.png";
import Header from "../../components/Header";
import { Link } from "react-router-dom";
import WebString from "../../value/WebString";
import Footer from "../../components/Footer";
const ContactUS = () => {
  const us = [
    { label: "تلفن اطلاع رسانی :", title: WebString.string.PHONE_NUMBER },
    { label: "آدرس :", title: WebString.string.ADDRESS },
    { label: "پیام رسان ها  :", Messenger: [{ icon: tlIcon, alt: "تلگرام", to: WebString.string.TELEGRAM_LOCATION }] },
  ];
  return (
    <Fragment>
      <div className="us-container">
        <Header />
        <main className="main-wrapper">
          <div className="web-type-wrapper fade-in-up">
            <div class="us-wrapper">
              <h1>تماس با ما</h1>
              <p>{WebString.string.CONTACT_US_P1} </p>
              <p>{WebString.string.CONTACT_US_P2}</p>
              <ul className="us-ul-li">
                {us.map((u, index) => {
                  return (
                    <li key={index}>
                      <label>{u.label}</label>
                      {u.Messenger ? (
                        u.Messenger.map((Messenger, index) => {
                          return (
                            <a target={"_blank"} key={index} href={Messenger.to}>
                              <img src={Messenger.icon} alt={Messenger.alt} />
                            </a>
                          );
                        })
                      ) : (
                        <span>{u.title}</span>
                      )}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </main>
      </div>
      <Footer />
    </Fragment>
  );
};

export default ContactUS;
