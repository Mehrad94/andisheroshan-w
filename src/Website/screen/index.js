import React from "react";
import "./index.scss";
import WebSiteMain from "../components/WebSiteMain";
import Footer from "../components/Footer";

const Website = props => {
  return (
    <div className="web">
      <React.Fragment>
        <WebSiteMain />
        <Footer />
      </React.Fragment>
    </div>
  );
};

export default Website;
