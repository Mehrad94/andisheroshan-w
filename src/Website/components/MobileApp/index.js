import React from "react";
import "./index.scss";
import SliderScrollFull from "../SliderScrollFull";

const MobileApp = props => {
  return (
    <div className="mobile-app">
      <div className="mobile-screen-container">
        <div className="mobile-screen">{props.children}</div>
      </div>
    </div>
  );
};

export default MobileApp;
