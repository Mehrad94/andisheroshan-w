import React, { useRef, useEffect, useState } from "react";
import "./index.scss";
import MobileApp from "../MobileApp";
const MainSection = props => {
  const { direction, subtitle, description, image, color } = props;
  const [sectionFade, setSectionFade] = useState(false);
  const sectionRef = useRef(null);
  const scroll = () => {
    // var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    // var scrolled = (winScroll / height) * 100;
    // document.getElementById("myBar").style.width = scrolled + "%";
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;

    const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    // console.log(height, windowScroll);

    if (sectionRef.current) {
      if (windowScroll + height * 1.07 > sectionRef.current.offsetTop + height) {
        //console.log("true");
        if (!sectionFade) {
          setSectionFade(true);
          if (height <= windowScroll) {
            // console.log("trueeee");
            // setSectionFade(true);
          }
        }
      }
    }
  };

  useEffect(() => {
    if (!sectionFade) {
      window.addEventListener("scroll", scroll);
    }
  });
  // var TopscrollTo = function () {
  //   if(window.scrollY!=0) {
  //     setTimeout(function() {
  //       window.scrollTo(0,window.scrollY-30);
  //       TopscrollTo();
  //     }, 5);
  //   }
  // }
  let dir = false;
  let containerClassname = null;
  if (direction === "ltr") dir = "row-reverse";
  if (sectionFade) {
    containerClassname = "opacity-Fade-in-adn-slide-top ";
  }
  return (
    <div ref={sectionRef} style={{ flexDirection: dir, visibility: sectionFade ? "" : "hidden" }} className={`section-wraper ${containerClassname}`}>
      <div className="section-title">
        <h3 style={{ color: color }}>{subtitle}</h3>
        <p>{description}</p>
      </div>
      <div className="section-images">
        <img style={{ height: "100%" }} src={image} alt={"andishe rooshan"} />
      </div>
    </div>
  );
};

export default MainSection;
