const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_SETTING = GS_PANEL_ADMIN_TITLE + "/setting";
const GS_ADMIN_SETTING_WEB = GS_PANEL_ADMIN_TITLE + "/web";
// ========================================================================= dashboard
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
// ========================================================================= course
const GS_ADMIN_COURSES = GS_PANEL_ADMIN_TITLE + "/courses";
const GS_ADMIN_ADD_COURSE = GS_PANEL_ADMIN_TITLE + "/addCourse";
const GS_ADMIN_SHOW_COURSES = GS_PANEL_ADMIN_TITLE + "/courses";
// ========================================================================= category
const GS_ADMIN_CATEGORIES = GS_PANEL_ADMIN_TITLE + "/categories";
// ========================================================================= blog
const GS_ADMIN_BLOG = GS_PANEL_ADMIN_TITLE + "/blog";
const GS_ADMIN_ADD_BLOG = GS_PANEL_ADMIN_TITLE + "/addBlog";
// ========================================================================= slider
const GS_ADMIN_SLIDER = GS_PANEL_ADMIN_TITLE + "/slider";
const GS_ADMIN_ADD_SLIDER = GS_PANEL_ADMIN_TITLE + "/addSlider";
// ========================================================================= login
const GS_ADMIN_LOGIN = GS_PANEL_ADMIN_TITLE + "/login";
// ========================================================================= user
const GS_ADMIN_USERS = GS_PANEL_ADMIN_TITLE + "/user";
// ========================================================================= seminar
const GS_ADMIN_ADD_SEMINAR = GS_PANEL_ADMIN_TITLE + "/addSeminar";
const GS_ADMIN_SEMINAR = GS_PANEL_ADMIN_TITLE + "/seminar";
// =========================================================================  audio book
const GS_ADMIN_AUDIO_BOOK = GS_PANEL_ADMIN_TITLE + "/audioBook";
const GS_ADMIN_ADD_AUDIO_BOOK = GS_PANEL_ADMIN_TITLE + "/addAudioBook";
// ==========================================================================  introduction job
const GS_ADMIN_INTRODUCTION_JOBS = GS_PANEL_ADMIN_TITLE + "/introductionJob";
const GS_ADMIN_ADD_INTRODUCTION_JOBS = GS_PANEL_ADMIN_TITLE + "/addIntroductionJob";
// ==========================================================================   admin
const GS_ADMIN_ADMIN = GS_PANEL_ADMIN_TITLE + "/admin";
const GS_ADMIN_ADD_ADMIN = GS_PANEL_ADMIN_TITLE + "/addAdmin";
// ==========================================================================   report

const GS_ADMIN_REPORTS = GS_PANEL_ADMIN_TITLE + "/report";

const routes = {
  GS_PANEL_ADMIN_TITLE,
  // ======================================= dashboard
  GS_ADMIN_DASHBOARD,
  // ======================================= course
  GS_ADMIN_ADD_COURSE,
  GS_ADMIN_COURSES,
  GS_ADMIN_SHOW_COURSES,
  // ======================================= category
  GS_ADMIN_CATEGORIES,
  GS_ADMIN_SETTING,
  // ======================================= setting
  GS_ADMIN_SETTING_WEB,
  // ======================================= blog
  GS_ADMIN_BLOG,
  GS_ADMIN_ADD_BLOG,
  // ======================================= slider
  GS_ADMIN_SLIDER,
  GS_ADMIN_ADD_SLIDER,
  // ======================================= login
  GS_ADMIN_LOGIN,
  // ======================================= user
  GS_ADMIN_USERS,
  // ======================================= seminar
  GS_ADMIN_SEMINAR,
  GS_ADMIN_ADD_SEMINAR,
  // ======================================= audio book
  GS_ADMIN_AUDIO_BOOK,
  GS_ADMIN_ADD_AUDIO_BOOK,
  // ======================================= introduction job
  GS_ADMIN_INTRODUCTION_JOBS,
  GS_ADMIN_ADD_INTRODUCTION_JOBS,
  // =======================================  admin
  GS_ADMIN_ADMIN,
  GS_ADMIN_ADD_ADMIN,
  // =======================================  report

  GS_ADMIN_REPORTS,
};

export default routes;
