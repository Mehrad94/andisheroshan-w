const BASE_URL = "https://andisheroshan.com/api/v1";
const ADMIN = "/admin";
const COURSE = ADMIN + "/course";
const COURSE_SEARCH = COURSE + "/s";
const SECTION_COURSE = ADMIN + "/section";

const VOICE_UPLOAD = ADMIN + "/voiceUpload";
const VIDEO_UPLOAD = ADMIN + "/videoUpload";
const UPLOAD = ADMIN + "/upload";

const CATEGORY = ADMIN + "/category";

const BLOG = ADMIN + "/blog";
const BLOG_SEARCH = BLOG + "/s";

const SLIDER = ADMIN + "/slider";

const LOGIN = ADMIN + "/login";

const USER = ADMIN + "/user";

const AUDIO_BOOK = ADMIN + "/audioBook";
const AUDIO_BOOK_SECTION = AUDIO_BOOK + "/section";

const SEMINAR = ADMIN + "/seminar";
const SEMINAR_SECTION = SEMINAR + "/section";

const INTRODUCTION_JOB = ADMIN + "/jobs";
const INTRODUCTION_JOB_SECTION = INTRODUCTION_JOB + "/section";

const ADMINS = ADMIN + "/admins";
const ADMIN_SEARCH = ADMINS + "/s";

const REPORT = ADMIN + "/report/accountant";

const ApiString = {
  COURSE,
  COURSE_SEARCH,
  SECTION_COURSE,
  VOICE_UPLOAD,
  VIDEO_UPLOAD,
  UPLOAD,
  CATEGORY,
  SLIDER,
  BLOG,
  BLOG_SEARCH,
  LOGIN,
  USER,
  AUDIO_BOOK,
  AUDIO_BOOK_SECTION,
  SEMINAR,
  SEMINAR_SECTION,
  INTRODUCTION_JOB,
  INTRODUCTION_JOB_SECTION,
  ADMINS,
  ADMIN_SEARCH,
  REPORT,
};
export default ApiString;
