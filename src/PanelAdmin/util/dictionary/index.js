import React from "react";

const dictionary = (text) => {
  let translated;

  switch (text) {
    case "BLOG":
      translated = "بلاگ";
      break;
    case "COURSE":
      translated = "دوره";
      break;
    case "Course":
      translated = "دوره";
      break;
    case "Seminar":
      translated = "سمینار";
      break;
    case "AudioBook":
      translated = "کتاب صوتی";
      break;
    case "Publisher":
      translated = "ناشر ";
      break;
    case "Admin":
      translated = "ادمین ";
      break;

    default:
      break;
  }
  return translated;
};

export default dictionary;
