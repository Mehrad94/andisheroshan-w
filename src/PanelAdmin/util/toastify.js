import React from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const toastify = async (text, type) => {
  toast.configure();
  if (type === "error") {
    // alert("error");
    await toast.error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");

    await toast.success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};
export default toastify;
