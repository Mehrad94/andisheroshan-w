import React from "react";
import { Redirect, Route } from "react-router-dom";
import checkAuth from "./chechkAuth";
import routes from "../value/routes";

const ProtectedRoute = ({ component: Component, ...rest }) => {
  // //console.log({ ...rest });

  return (
    <Route
      {...rest}
      render={props => {
        // //console.log({ props });
        if (checkAuth()) return <Component {...props} />;
        else return <Redirect to={routes.GS_ADMIN_LOGIN} />;
      }}
    />
  );
};

export default ProtectedRoute;
