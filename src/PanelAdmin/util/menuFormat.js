import routes from "../value/routes";
import PanelString from "../value/PanelString";
const menuData = [
  {
    title: "عمومی",
    menus: [
      {
        route: routes.GS_ADMIN_DASHBOARD,
        menuTitle: PanelString.Strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: " icon-gauge",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: PanelString.Strings.COURSES,
        menuIconImg: false,
        menuIconClass: " icon-videocam",
        subMenu: [
          {
            title: PanelString.Strings.SEE_COURSES,
            route: routes.GS_ADMIN_SHOW_COURSES,
          },
          {
            title: PanelString.Strings.ADD_COURSE,
            route: routes.GS_ADMIN_ADD_COURSE,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.BLOGS,
        menuIconImg: false,
        menuIconClass: "icon-desktop",
        subMenu: [
          {
            title: PanelString.Strings.SEE_BLOGS,
            route: routes.GS_ADMIN_BLOG,
          },
          {
            title: PanelString.Strings.ADD_BLOG,
            route: routes.GS_ADMIN_ADD_BLOG,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.AUDIO_BOOK,
        menuIconImg: false,
        menuIconClass: "icon-volume-up",
        subMenu: [
          {
            title: PanelString.Strings.SEE_AUDIO_BOOKS,
            route: routes.GS_ADMIN_AUDIO_BOOK,
          },
          {
            title: PanelString.Strings.ADD_AUDIO_BOOK,
            route: routes.GS_ADMIN_ADD_AUDIO_BOOK,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.SLIDERS,
        menuIconImg: false,
        menuIconClass: "icon-picture",
        subMenu: [
          {
            title: PanelString.Strings.SEE_SLIDERS,
            route: routes.GS_ADMIN_SLIDER,
          },
          {
            title: PanelString.Strings.ADD_SLIDER,
            route: routes.GS_ADMIN_ADD_SLIDER,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.CATEGORIES,
        menuIconImg: false,
        menuIconClass: "icon-buffer",
        subMenu: [
          {
            title: PanelString.Strings.SEE_CATEGORY,
            route: routes.GS_ADMIN_CATEGORIES,
          },
        ],
      },

      {
        route: false,
        menuTitle: PanelString.Strings.SEMINAR,
        menuIconImg: false,
        menuIconClass: " icon-edit-1",
        subMenu: [
          {
            title: PanelString.Strings.SEE_SEMINAR,
            route: routes.GS_ADMIN_SEMINAR,
          },
          {
            title: PanelString.Strings.ADD_SEMINAR,
            route: routes.GS_ADMIN_ADD_SEMINAR,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.INTRODUCTION_JOB,
        menuIconImg: false,
        menuIconClass: "icon-address-card-o",
        subMenu: [
          {
            title: PanelString.Strings.SEE_INTRODUCTION_JOBS,
            route: routes.GS_ADMIN_INTRODUCTION_JOBS,
          },
          {
            title: PanelString.Strings.ADD_INTRODUCTION_JOB,
            route: routes.GS_ADMIN_ADD_INTRODUCTION_JOBS,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.ADMIN,
        menuIconImg: false,
        menuIconClass: "icon-handshake-o",
        subMenu: [
          {
            title: PanelString.Strings.SEE_ADMIN,
            route: routes.GS_ADMIN_ADMIN,
          },
          {
            title: PanelString.Strings.ADD_ADMIN,
            route: routes.GS_ADMIN_ADD_ADMIN,
          },
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.MEMBER,
        menuIconImg: false,
        menuIconClass: " icon-print",
        subMenu: [
          {
            title: PanelString.Strings.SEE_MEMBERS,
            route: routes.GS_ADMIN_USERS,
          },
          // {
          //   title: PanelString.Strings.ADD_SLIDER,
          //   route: pageRoutes.GS_ADMIN_ADD_SLIDER
          // }
        ],
      },
      {
        route: false,
        menuTitle: PanelString.Strings.REPORT,
        menuIconImg: false,
        menuIconClass: "icon-users",
        subMenu: [
          {
            title: PanelString.Strings.SEE_REPORTS,
            route: routes.GS_ADMIN_REPORTS,
          },
          // {
          //   title: PanelString.Strings.ADD_SLIDER,
          //   route: pageRoutes.GS_ADMIN_ADD_SLIDER
          // }
        ],
      },
    ],
  },
];

export default menuData;
