import toastify from "../toastify";
import { post } from "../../api";

const handelOnchange = async (name, value, update, type, files, validPlay, validImage, setDetails, setLoading) => {
  console.log({ name, value, update, type, validPlay, validImage, setDetails });

  if (type === "file") {
    if (validImage) {
      if (files.type.includes("image")) await post.imageUpload(files, setLoading, update, name, setDetails);
      else {
        value = "";
        toastify("فایل شما نباید " + files.type + " باشد", "error");
      }
      // setDetails(prev => ({ ...prev, imageName: value }));
    } else if (validPlay) {
      if (validPlay === "VOICE") {
        if (files.type.includes("audio")) await post.voiceUpload(files, setLoading, update, name, setDetails);
        else {
          value = "";
          toastify("فایل شما نباید " + files.type + " باشد", "error");
        }
      } else if (validPlay === "VIDEO") {
        if (files.type.includes("video")) await post.videoUpload(files, setLoading, update, name, setDetails);
        else {
          value = "";
          toastify("فایل شما نباید " + files.type + " باشد", "error");
        }
      }
      // setDetails(prev => ({ ...prev, playName: value }));
    }
  } else {
    update[name] = value;
  }
  setDetails(prev => ({ ...prev, progressPercentPlay: null, progressPercentImage: null }));
};
export default handelOnchange;
