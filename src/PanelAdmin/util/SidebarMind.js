import RealRoutes from "../Values/RealRoutes";
import PanelString from "../Values/PanelString";

const SideBarMind = {
	// ================== TOP SIDEBAR TITLE ==================
	sidebarPublic: [
		{
			title: PanelString.Strings.DASHBOARD,
			href: RealRoutes.GS_ADMIN_DASHBOARD,
			iconClass: "icon-photo",
			name: RealRoutes.GS_ADMIN_DASHBOARD
		}
	],
	// ================== TOP SIDEBAR TITLE END==================

	// ================== BOTTOM SIDEBAR TITLE ==================

	sidebarApplication: [
		{
			title: PanelString.Strings.COURSES,
			href: false,
			iconClass: " icon-videocam",
			name: RealRoutes.GS_ADMIN_COURSES,
			child: [
				{
					title: PanelString.Strings.SEE_COURSES,
					href: RealRoutes.GS_ADMIN_SHOW_COURSES
				},
				{
					title: PanelString.Strings.ADD_COURSE,
					href: RealRoutes.GS_ADMIN_ADD_COURSES
				}
			]
		},
		{
			title: PanelString.Strings.SETTING,
			href: false,
			iconClass: " icon-certificate ",

			name: RealRoutes.GS_ADMIN_SETTING,
			child: [
				{
					title: PanelString.Strings.SETTING,
					href: RealRoutes.GS_ADMIN_SETTING_WEB
				}
			]
		}
	]
	// ================== BOTTOM SIDEBAR TITLE END==================
};
export default SideBarMind;
