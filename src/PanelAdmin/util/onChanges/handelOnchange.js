const handelOnchange = async ({ event, data, setData, setState, setLoading, fileType, validName, checkValidity, updateObject, uploadChange, changeState }) => {
  let changeValid,
    updatedForm,
    updatedFormElement = {};
  let formIsValid = true;

  let value = data.Form[event.name].value;
  let update = { ...value };
  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  console.log({ typeofData, value });

  typeofData === "object" && value != null ? (value.length && value.length >= 0 ? (isArray = true) : (isObject = true)) : (isString = true);
  const remove = index => value.splice(index, 1)[0];
  const push = (val, newVal) => (newVal != undefined ? val.push(newVal) : "");
  console.log({ eventValue, value });

  if (event.type === "file") {
    const uploadFile = await uploadChange({ event, setLoading, fileType, setState, valid: data.Form[event.name].kindOf });
    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);
      else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);
  else if (isObject) {
    value = eventValue;
    if (event.child) value = update[event.child] = eventValue;
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;
  else checkValidValue = value;

  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
    touched: true
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], { valid: true });
    updatedForm = updateObject(data.Form, { ...updatedFormElement, [validName]: changeValid });
  } else if (changeState) {
    let updateState = updateObject(data.Form[changeState.name], { [changeState.argoman]: changeState.value });
    updatedForm = updateObject(data.Form, { ...updatedFormElement, [changeState.name]: updateState });
  } else updatedForm = updateObject(data.Form, { ...updatedFormElement });

  console.log({ updatedFormElement, updatedForm });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

  return setData({ Form: updatedForm, formIsValid: formIsValid });
};
export default handelOnchange;
