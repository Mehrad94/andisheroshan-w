import React from "react";
import "./index.scss";
const BackgrandCover = props => {
	return <div id="coverContainer" onClick={props.onClick} className={props.fadeIn ? " fadeIn" : " fadeOut"}></div>;
};

export default BackgrandCover;
