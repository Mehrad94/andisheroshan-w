const addSeminarSection = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    video: {
      label: "آپلود فایل تصویری :",
      elementType: "inputFile",
      kindOf: "video",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addSeminarSection;
