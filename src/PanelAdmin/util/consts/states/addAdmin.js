const addAdmin = {
  Form: {
    role: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "ادمین", value: "Admin" },
        { name: "ناشر", value: "Publisher" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    fullName: {
      label: "نام و نام خانوادگی:",
      elementType: "input",
      elementConfig: {
        placeholder: "نام و نام خانوادگی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    pubType: {
      label: " مسولیت در :",

      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: " مسولیت در",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    phoneNumber: {
      label: "شماره همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "شماره همراه",
      },
      value: "",
      validation: {
        required: true,
        isNumeric: true,
        isMobile: true,
        minLength: 11,
        maxLength: 11,
      },
      valid: false,
      touched: false,
    },
    password: {
      label: "رمز عبور:",
      elementType: "input",
      elementConfig: {
        placeholder: "رمز عبور",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addAdmin;
