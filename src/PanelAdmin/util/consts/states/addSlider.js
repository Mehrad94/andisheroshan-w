const addSlider = {
  Form: {
    type: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [
        { name: "بلاگ", value: "BLOG" },
        { name: "دوره", value: "COURSE" }
      ],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    course: {
      label: "دوره :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    blog: {
      label: " بلاگ :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addSlider;
