import addSeminar from "./addSeminar";
import addCategory from "./addCategory";
import addSlider from "./addSlider";
import addBlog from "./addBlog";
import upload from "./upload";
import addSection from "./addSection";
import addCourse from "./addCourse";
import addAudioBook from "./addAudioBook";
import addIntroductionJob from "./addIntroductionJob";
import addAudioBookSection from "./addAudioBookSection";
import addSeminarSection from "./addSeminarSection";
import addIntroductionJobSection from "./addIntroductionJobSection";
import addAdmin from "./addAdmin";

const states = {
  addSeminar,
  addCategory,
  addSlider,
  addBlog,
  upload,
  addSection,
  addCourse,
  addAudioBook,
  addIntroductionJob,
  addAudioBookSection,
  addSeminarSection,
  addIntroductionJobSection,
  addAdmin
};
export default states;
