const addCourse = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    aboutCourse: {
      label: "توضیحات کلی دوره:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات کلی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    price: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی",
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },

    publisher: {
      label: "ناشر :",

      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ناشر",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    cover: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addCourse;
