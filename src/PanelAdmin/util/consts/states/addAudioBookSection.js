const addAudioBookSection = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    voice: {
      label: "آپلود صوت :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addAudioBookSection;
