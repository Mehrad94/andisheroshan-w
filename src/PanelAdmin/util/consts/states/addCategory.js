const owner = {
  Form: {
    title: {
      label: "عنوان  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default owner;
