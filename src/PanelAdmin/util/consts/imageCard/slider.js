import dictionary from "../../dictionary";
const slider = data => {
  const cardImageFormat = [];
  for (let index in data) {
    let courseOrBlog = data[index].type === "BLOG" ? data[index].blog : data[index].type === "COURSE" ? data[index].course : "";

    cardImageFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index].image },
      title: dictionary(data[index].type),
      description: courseOrBlog && courseOrBlog.title
    });
  }
  return cardImageFormat;
};

export default slider;
