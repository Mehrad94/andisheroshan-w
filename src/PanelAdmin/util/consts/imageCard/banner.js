import dictionary from "../../dictionary";

const banner = data => {
  console.log({ datas: data });

  const cardImageFormat = [];
  for (let index in data) {
    console.log(data);

    let discountOrClubData = data[index].parentType === "DISCOUNT" ? data[index].discount : data[index].parentType === "CLUB" ? data[index].club : "";
    cardImageFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].image },
      title: dictionary(data[index].parentType),
      description: discountOrClubData && discountOrClubData.title
    });
  }
  return cardImageFormat;
};

export default banner;
