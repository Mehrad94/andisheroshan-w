import ShowCategory from "./ShowCategory";
import showScenario from "./ShowScenario";
import ShowScenarioDataInModal from "./ShowScenarioDataInModal";
import ShowScenarioDataInModalTwo from "./ShowScenarioDataInModalTwo";
import showOwner from "./ShowOwner";
import reportsSoldSeminar from "./reportsSoldSeminar";
import ShowClub from "./ShowClub";
import showTransaction from "./ShowTransactions";
import transactionDiscount from "./transactionDiscount";
import members from "./members";
import memberTransaction from "./memberTransaction";
import memberDiscount from "./memberDiscount";
const table = {
  memberDiscount,
  memberTransaction,
  members,
  transactionDiscount,
  showTransaction,
  ShowClub,
  reportsSoldSeminar,
  showOwner,
  showScenario,
  ShowCategory,
  ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo,
};
export default table;
