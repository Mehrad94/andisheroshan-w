import React from "react";

const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;

  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? (thead = headNotGift) : (thead = headGift);

  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? (body = bodyNotGift) : (body = bodyGift);
    tbody.push({ data: body, style: {} });
  }
  return [thead, tbody];
};

export default ShowScenarioDataInModal;
