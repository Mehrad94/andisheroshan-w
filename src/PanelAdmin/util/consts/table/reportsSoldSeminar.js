import React from "react";
import formatMoney from "../../formatMoney";

const ShowDiscount = (data) => {
  const thead = ["#", "سمینار", "مشتری", "تاریخ", "قیمت", "ناشر سمینار"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
    let seminar = data[index].seminar ? data[index].seminar : NotEntered;
    let seminarTitle = seminar.title ? seminar.title : NotEntered;
    let publisher = data[index].publisher ? data[index].publisher : NotEntered;
    let publisherName = publisher.fullName ? publisher.fullName : NotEntered;
    let customer = data[index].customer ? data[index].customer : NotEntered;
    let customerName = customer.fullName ? customer.fullName : NotEntered;
    let customerPhoneNumber = customer.phoneNumber ? customer.phoneNumber : NotEntered;

    let price = data[index].price ? formatMoney(data[index].price) : "0";

    tbody.push({
      data: [seminarTitle, publisherName, customerName, customerPhoneNumber, date, price],
      style: {},
    });
  }
  return [thead, tbody];
};

export default ShowDiscount;
