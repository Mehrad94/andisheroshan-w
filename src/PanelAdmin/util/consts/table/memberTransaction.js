import React from "react";
import formatMoney from "../../formatMoney";

const memberTransaction = (data) => {
  console.log({ data });

  // let data =datas.discount
  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? <span style={{ color: "green" }}>{"پرداخت شده"}</span> : <span style={{ color: "red" }}> {"پرداخت نشده"}</span>;
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
    let totalPrice = dataIndex.totalPrice ? formatMoney(dataIndex.totalPrice) : "0";

    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {},
    });
  }
  return [thead, tbody];
};

export default memberTransaction;
