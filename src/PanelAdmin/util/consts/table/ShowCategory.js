import React from "react";

const ShowCategory = (Categories) => {
  const thead = ["#", "عکس", "عنوان انگلیسی", "عنوان فارسی", "زیر مجموعه", "وزن", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < Categories.length; index++) {
    let NotEntered = "وارد نشده";
    let subCategory = Categories[index].subCategories.length ? Categories[index].subCategories : "ندارد";
    let image = Categories[index].image ? Categories[index].image : NotEntered;
    let titleFa = Categories[index].titleFa ? Categories[index].titleFa : NotEntered;
    let titleEn = Categories[index].titleEn ? Categories[index].titleEn : NotEntered;
    let weight = Categories[index].weight ? Categories[index].weight : NotEntered;
    tbody.push({ data: [image, titleEn, titleFa, subCategory, weight, { option: { edit: true } }], style: {} });
  }
  return [thead, tbody];
};

export default ShowCategory;
