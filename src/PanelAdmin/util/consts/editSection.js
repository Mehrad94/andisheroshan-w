const editSection = {
  type: {
    label: "نوع قسمت",
    type: "button",

    text: [
      { title: "تصویری", value: "VIDEO" },
      { title: "صوتی", value: "VOICE" }
    ]
  },
  title: { label: "عنوان", type: "text" },
  description: { label: "توضیحات", type: "text" },
  upload: { label: "آپلود فایل", type: "file" }
};
export default editSection;
