import Cookie from "js-cookie";

const checkAuth = () => {
  return Cookie.get("andisheToken") !== undefined;
};

export default checkAuth;
