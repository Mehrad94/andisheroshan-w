import React from "react";

const Textarea = props => {
  const { onTextChange, name, inputval, state, disabled } = props;
  return <textarea name={name} disabled={disabled} onChange={onTextChange} value={inputval ? inputval : ""} />;
};

export default Textarea;
