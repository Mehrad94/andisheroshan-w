import addSectionsArray from "./addSectionsArray";
import addSliderArray from "./addSliderArray";
import editBlogArray from "./blog/editBlogArray";

const inputArray = { addSectionsArray, addSliderArray, editBlogArray };
export default inputArray;
