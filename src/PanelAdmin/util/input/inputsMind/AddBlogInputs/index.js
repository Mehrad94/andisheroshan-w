import React, { useState } from "react";
import FormInputsBox from "../../FormInputsBox";
import handelOnchange from "../../../onChanges/handelOnchange";

const AddBlogInputs = props => {
  const { value, entry, update, setData, setState, data, state, edit } = props;

  const [loading, setLoading] = useState(false);
  const _ChangeType = (title, name, value) => {
    setState({ activedType: title });
    setData({ type: value, title: "", description: "" });
  };
  const _handelOnchange = async (e, image) => {
    let name = e.currentTarget.name;
    let type = e.currentTarget.type;
    let value = e.currentTarget.value;
    let files;
    if (type === "file") {
      files = e.currentTarget.files[0];
      if (files.type.includes("video")) name = "dataUrl";
      else if (files.type.includes("audio")) name = "dataUrl";
      else if (files.type.includes("image")) name = "cover";
    }

    await handelOnchange(name, value, update, type, files, data.type, image, setState, setLoading);
    setState(prev => ({ ...prev, progressPercent: null }));
    setData(update);
  };
  const elements = entry.map((info, index) => {
    return (
      <FormInputsBox
        key={index}
        ChangeType={_ChangeType}
        onChange={e => _handelOnchange(e, info.name === "cover")}
        Informations={info}
        value={data ? data[info.name] : null}
        state={state}
        progressPercentFile={info.name === "cover" ? state.progressPercentImage : state.progressPercentPlay}
        disabledInput={loading ? true : data.type ? false : true}
        btnDisabled={loading || edit ? true : ""}
      />
    );
  });
  return <div className="subjectModal-AddInformation">{elements}</div>;
};

export default AddBlogInputs;
