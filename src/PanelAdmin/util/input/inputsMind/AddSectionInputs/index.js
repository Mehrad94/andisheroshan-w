import React, { useState, useEffect } from "react";
import "./index.css";
import { post } from "../../../../api";
import FormInputsBox from "../../FormInputsBox";
import toastify from "../../../toastify";
import handelOnchange from "../../../onChange/handelOnchange";
const AddSectionInputs = props => {
  const { value, showModal, onHideModal, inputsData, acceptedTitle, Informations, entry, edit } = props;
  const [data, setData] = useState({ type: "", title: "", description: "" });
  const [state, setState] = useState({
    activedType: null,
    fileName: null,
    progressPercent: null
  });
  console.log({ edit });

  const [cancelUpload, setCancelUpload] = useState(false);

  const [loading, setLoading] = useState(false);
  let update = { ...data };

  useEffect(() => {
    if (!showModal) {
      setData({ type: "", title: "", description: "" });
      setState({
        activedType: null,
        fileName: null,
        progressPercent: null
      });
    } else {
      if (edit) {
        if (edit.type === "VIDEO") {
          if (edit.video) setData({ type: edit.type, title: edit.title, description: edit.description, video: edit.video });
          else setData({ type: edit.type, title: edit.title, description: edit.description, dataUrl: edit.dataUrl });
          setState({ ...state, activedType: "تصویری" });
        }
        if (edit.type === "VOICE") {
          if (edit.voice) setData({ type: edit.type, title: edit.title, description: edit.description, voice: edit.voice });
          else setData({ type: edit.type, title: edit.title, description: edit.description, dataUrl: edit.dataUrl });
          setState({ ...state, activedType: "صوتی" });
        }
      }
    }
  }, [showModal]);

  // useEffect(() => {

  // }, [edit]);

  // ================================================ const
  const onHide = () => {
    onHideModal();
  };
  const _ChangeType = (title, name, value) => {
    setState({ activedType: title });
    setData({ type: value, title: "", description: "" });
  };
  const _handelOnchange = async (e, image) => {
    let name = e.currentTarget.name;
    let type = e.currentTarget.type;
    let value = e.currentTarget.value;
    let files;
    if (type === "file") {
      files = e.currentTarget.files[0];
      if (files.type.includes("video")) name = "video";
      else if (files.type.includes("audio")) name = "voice";
      else if (files.type.includes("image")) name = "image";
    }

    await handelOnchange(name, value, update, type, files, data.type, image, setState, setLoading);
    setState(prev => ({ ...prev, progressPercent: null }));
    setData(update);
  };

  const returnData = () => {
    if (data) {
      if (data.description === "" && data.title === "" && data.type === "" && (data.voice === "" || data.video === "")) toastify("اطلاعات کامل نمی باشد", "error");
      else if (data.voice || data.video) {
        inputsData(data);
        onHideModal();
      } else toastify("اطلاعات کامل نمی باشد", "error");
    } else toastify("اطلاعات کامل نمی باشد", "error");
  };

  const elements = entry.map((info, index) => {
    return (
      <FormInputsBox
        onChange={e => _handelOnchange(e)}
        ChangeType={_ChangeType}
        Informations={info}
        value={info.name === "upload" ? data["voice"] || data["video"] : data ? data[info.name] : null}
        state={state}
        key={index}
        progressPercentFile={state.progressPercentPlay}
        disabledInput={loading ? true : data.type ? false : true}
        btnDisabled={loading || edit ? true : ""}
      />
    );
  });

  return (
    <div className="subjectModal-AddInformation">
      {elements}
      <div className="btn-submited-container">
        <div className="submitedBtn">
          <button disabled={loading} onClick={returnData}>
            {acceptedTitle}{" "}
          </button>
        </div>
        <div className="submitedBtn closed">
          <button onClick={!loading ? onHide : ""}>انصراف</button>
        </div>
      </div>
    </div>
  );
};

export default AddSectionInputs;
