import React, { useState, useEffect } from "react";
import FormInputsBox from "../../FormInputsBox";
import toastify from "../../../toastify";
import handelOnchange from "../../../onChange/handelOnchange";
const InputsType = props => {
  const { showModal, onHideModal, inputsData, acceptedTitle, entry } = props;

  const [data, setData] = useState(false);
  const [state, setState] = useState(false);
  const [cancelUpload, setCancelUpload] = useState(false);
  const [loading, setLoading] = useState(false);
  let update = { ...data };
  console.log({ entry });

  useEffect(() => {
    if (!showModal) {
      setData(false);
      setState(false);
    }
  }, [showModal]);
  const _handelOnchange = async (e, image) => {
    let name = e.currentTarget.name;
    let type = e.currentTarget.type;
    let value = e.currentTarget.value;
    let files;
    if (type === "file") {
      files = e.currentTarget.files[0];
      // if (files.type.includes("video")) name = "video";
      // else if (files.type.includes("audio")) name = "voice";
      // else if (files.type.includes("image")) name = "image";
    }

    await handelOnchange(name, value, update, type, files, entry.valid, image, setState, setLoading);
    setState(prev => ({ ...prev, progressPercent: null }));
    setData(update);
  };
  const returnData = () => {
    if (data) {
      if (data === "") toastify("اطلاعات کامل نمی باشد", "error");
      else if (data) {
        console.log({ data });

        inputsData(data);
        onHideModal();
      } else toastify("اطلاعات کامل نمی باشد", "error");
    } else toastify("اطلاعات کامل نمی باشد", "error");
  };

  const elements = (
    <FormInputsBox
      onChange={e => _handelOnchange(e, entry.name === "cover" || entry.name === "image")}
      Informations={entry}
      value={data ? data[entry.name] : null}
      state={state}
      progressPercentFile={entry.name === "cover" || entry.name === "image" ? state.progressPercentImage : state.progressPercentPlay}
      disabledInput={false}
    />
  );

  return (
    <div className="subjectModal-AddInformation">
      {elements}
      <div className="btn-submited-container">
        <div className="submitedBtn">
          <button disabled={loading} onClick={returnData}>
            {acceptedTitle}
          </button>
        </div>
        <div className="submitedBtn closed">
          <button onClick={!loading ? onHideModal : ""}>انصراف</button>
        </div>
      </div>
    </div>
  );
};

export default InputsType;
