import React from "react";

import { ButtonGroup, Button } from "react-bootstrap";
import InputFile from "../types/file";
import InputText from "../types/text";
import Textarea from "../types/Textarea";
const FormInputsBox = props => {
  const { Informations, state, ChangeType, onChange, cancelUpload, value, progressPercentFile, disabledInput, btnDisabled } = props;

  const elements = (
    <React.Fragment>
      <label>{Informations.value.label}</label>
      {Informations.value.type === "file" ? (
        <InputFile
          inputLabel={"افزودن فایل"}
          progress={progressPercentFile}
          inputval={value}
          {...Informations}
          onChange={onChange}
          disabled={disabledInput}
          cancelUpload={cancelUpload}
        />
      ) : Informations.value.type === "text" ? (
        <InputText disabled={disabledInput} inputType={"text"} inputval={value} {...Informations} onChange={onChange} />
      ) : Informations.value.type === "button" ? (
        <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
          {Informations.value.text.map((text, index) => {
            return (
              <Button
                disabled={btnDisabled}
                onClick={() => ChangeType(text.title, Informations.name, text.value)}
                style={{
                  backgroundColor: state ? (state.activedType === text.title ? "#07a7e3" : "") : ""
                }}
                key={index}
                variant="secondary"
              >
                {text.title}{" "}
              </Button>
            );
          })}
        </ButtonGroup>
      ) : Informations.value.type === "textarea" ? (
        <Textarea disabled={disabledInput} inputType={"text"} inputval={value} {...Informations} onTextChange={onChange} />
      ) : (
        ""
      )}
    </React.Fragment>
  );

  return elements;
};

export default FormInputsBox;
