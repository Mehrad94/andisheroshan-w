export const checkValidity = (value, rules, array) => {
  let isValid = true;
  console.log({ value, rules, array });

  if (!rules) {
    return true;
  }
  if (array) {
    return (isValid = value.length > 0);
  }
  if (rules.required) {
    isValid = value.trim() !== "" && isValid;
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }

  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isNumeric) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
  }
  if (rules.isPhone) {
    const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    isValid = pattern.test(value) && isValid;
  }
  if (rules.isMobile) {
    const pattern = /[0,9]{2}\d{9}/g;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isEn) {
    const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    isValid = pattern.test(value) && isValid;
  }
  if (rules.isFa) {
    const pattern = /^[\u0600-\u06FF\s]+$/;
    isValid = pattern.test(value) && isValid;
  }

  return isValid;
};
