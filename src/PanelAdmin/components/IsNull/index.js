import React from "react";
import "./index.scss";
const IsNull = (props) => {
  const { title } = props;
  return (
    <div className="Null-data">
      <span>{title}</span>
    </div>
  );
};

export default IsNull;
