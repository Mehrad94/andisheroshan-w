import React from "react";
import "./index.scss";
const SearchDropDown = props => {
  const { data, onClick } = props;
  return (
    data.length > 0 && (
      <div className="searched-dropDown">
        {data.map((searched, index) => {
          return (
            <div key={index} onClick={() => onClick(index)} className="serached-item">
              <div className="searched-image">
                <img src={searched.cover} alt="search title" />
              </div>
              <div className="searched-text">
                <span className="searched-title">{searched.title}</span>
                <span className="searched-description">{searched.description}</span>
              </div>
            </div>
          );
        })}
      </div>
    )
  );
};

export default SearchDropDown;
