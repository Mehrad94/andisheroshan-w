import React, { useState, useEffect, Fragment } from "react";
import "./index.scss";
import AcceptedChild from "./AcceptedChild";
const SearchDropDown = (props) => {
  const { dropDownData, onChange, accepted, disabled, checkSubmited, className, elementConfig, onKeyDown, staticTitle } = props;
  const [search, setSearch] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [acceptedSave, setAcceptedSave] = useState();
  const [typing, setTping] = useState(false);

  useEffect(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  useEffect(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave();
    setTping(false);
  }, [checkSubmited]);
  const handleBlur = (e) => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0)
      setTimeout(() => {
        setSearch([]);
      }, 300);
  };
  const handelOnChange = (e) => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
  };

  const clickedElement = (index) => {
    setAcceptedSave(search[index]);
    setSearchTitle(search[index].title);
    accepted(search[index].value);
  };

  const searchContainer = (
    <div className="text-search-dropDown">
      <div>
        <input onKeyDown={onKeyDown} disabled={disabled} value={searchTitle ? searchTitle : typing ? "" : staticTitle} onBlur={handleBlur} onChange={handelOnChange} className={className} {...elementConfig} />
        {search && search.length > 0 && (
          <div className="searched-dropDown felxRow">
            {search.map((searched, index) => {
              return (
                <div key={index} onClick={() => clickedElement(index)} className="serached-item">
                  <div className="searched-image">
                    <img src={searched.image} alt="search title" />
                  </div>
                  <div className="searched-text">
                    <span className="searched-title">{searched.title}</span>
                    <span className="searched-description">{searched.description}</span>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div>
        <AcceptedChild data={acceptedSave} />
      </div>
    </div>
  );
  return searchContainer;
};

export default SearchDropDown;
