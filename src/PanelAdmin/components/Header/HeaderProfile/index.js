import React, { useState, useEffect, useRef } from "react";
import profileImage from "../../../assets/Images/icons/user.png";
import "./index.scss";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

const HeaderProfile = () => {
  const [state, setState] = useState({
    showModal: false,
    clickedComponent: false,
  });
  const wrapperRef = useRef(null);

  const showModalprofile = () => {
    let noting = !state.showModal;
    setState((prev) => ({ ...prev, showModal: noting, clickedComponent: true }));
  };
  const logOut = () => Cookies.remove("andisheToken");
  const adminTitleModal = [
    { title: "پروفایل", iconClass: " icon-user-circle", href: "#", onClick: null },
    { title: "پیام ها", iconClass: "icon-mail", href: "#", value: 2, onClick: null },
    { title: "تنظیمات", iconClass: " icon-certificate ", href: "#", onClick: null },
    { title: "خروج", iconClass: "  icon-logout-1", href: "#", onClick: logOut },
  ];

  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) showModalprofile();
  };
  useEffect(() => {
    if (state.showModal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });
  const adminTitleModal_map = (
    <ul className={`profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`}>
      {adminTitleModal.map((admin, index) => {
        return (
          <li onClick={admin.onClick} key={index + "m"}>
            <i className={admin.iconClass}></i>
            <Link to={admin.href}>{admin.title}</Link>
            {admin.value ? <span className="show-modal-icon-value">{admin.value}</span> : ""}
          </li>
        );
      })}
    </ul>
  );
  return (
    <ul onClick={showModalprofile} ref={wrapperRef} className="panel-navbar-profile ">
      <li className="pointer hoverColorblack normalTransition">
        <div className="admin-profile-name  icon-up-dir"> ادمین</div>
        <div className="admin-profile-image">
          <img src={profileImage} alt="profile" />
        </div>
      </li>

      {adminTitleModal_map}
    </ul>
  );
};

export default HeaderProfile;
