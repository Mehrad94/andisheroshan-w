import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/Images/img/logo.png";
import MainMenu from "../../components/SideMenu/MainMenu";
import menuData from "../../util/menuFormat";
import "./index.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
const SideMenu = () => {
  return (
    <div className="panelAdmin-sideBar-container ">
      <PerfectScrollbar>
        <Link to={"/panelAdmin"} className={"lgHolder"}>
          <img src={logo} alt={"logo"} />
        </Link>
        <MainMenu mainMenus={menuData} />
      </PerfectScrollbar>
    </div>
  );
};

export default SideMenu;
