import React, { useState, useEffect } from "react";
import MenuTitle from "../MenuTitle";
import Menu from "../Menu";

const MainMenu = (props) => {
  const { mainMenus, sidebarList } = props;
  const [showLi, setShowLi] = useState("");
  const [selectedMenuTitle, setMenuTitle] = useState();

  useEffect(() => {
    checkMenu();
  }, []);
  useEffect(() => {
    if (window.history.back) checkMenu();
  }, [window.history.href]);

  const checkMenu = () => {
    mainMenus.map((menu) => {
      return menu.menus.map((menu) => {
        return menu.subMenu.map((subMenu) => {
          // console.log({ subMenu }, window.location.href.substr(window.location.href.indexOf("panelAdmin")));
          if (subMenu.route === "/" + window.location.href.substr(window.location.href.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
          }
        });
      });
    });
  };
  useEffect(() => {
    checkMenu();
  }, [window.location.href]);
  return mainMenus.map((mainMenu, index) => {
    return (
      <ul key={index + "moj"} ref={sidebarList}>
        <MenuTitle title={mainMenu.title} />
        <Menu menus={mainMenu.menus} showLi={showLi} setShowLi={setShowLi} selectedMenuTitle={selectedMenuTitle} setMenuTitle={setMenuTitle} />
      </ul>
    );
  });
};

export default MainMenu;
