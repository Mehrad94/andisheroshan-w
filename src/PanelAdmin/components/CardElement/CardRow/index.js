import React from "react";
import Star from "../../UI/Rating/Star";
import formatMoney from "../../../util/formatMoney";

const CardRow = props => {
  const { elementType, value, className, style, iconStyle, title } = props;
  let element;
  switch (elementType) {
    case "text":
      element = (
        <span title={title} style={{ ...style }}>
          {value}
        </span>
      );
      break;
    case "star":
      element = <Star rating={value} fixed size={"small"} />;

      break;
    case "price":
      element = (
        <span style={{ ...style }} className="s-c-card-price">
          {formatMoney(value)}
        </span>
      );
      break;
    case "icon":
      element = (
        <div className="cardPhonenumber-wrapper">
          {" "}
          <span style={{ ...style }}>{value}</span>
          <i style={{ ...iconStyle }} className={className}></i>
        </div>
      );
      break;
    case "district":
      element = (
        <div className="cardDistric-wrapper">
          {" "}
          <i className=" icon-location-1"></i>
          <span>{value}</span>
        </div>
      );
      break;
    default:
      break;
  }
  return element;
};

export default CardRow;
