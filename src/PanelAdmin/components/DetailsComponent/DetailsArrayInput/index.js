import React, { useState, useEffect } from "react";
import "./index.scss";
const DetailsArrayInput = ({ Info, sendNewVal, fieldName, label, toastify }) => {
  const [state, setState] = useState({
    change: false,
    StateInfo: [...Info]
  });
  useEffect(() => {
    setState(prev => ({
      ...prev,
      StateInfo: Info
    }));
  }, []);
  const _handelEdit = index => {
    setState(prev => ({
      ...prev,
      change: !state.change,
      StateInfo: [...Info]
    }));
  };

  // console.log(state.StateInfo, Info);

  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.StateInfo };
    sendNewVal(data);
    _handelEdit();
  };
  const _handelOnchange = (e, index) => {
    let newState = { ...state.StateInfo };
    newState[index] = e.currentTarget.value;
    let InfoChange = state.StateInfo;
    InfoChange[index] = newState[index];
    setState({ ...state, StateInfo: InfoChange });
  };

  return (
    <div className="card-details-row">
      <div className="about-title">
        <span>{label} :</span> <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>
        {state.change ? (
          <div className="btns-container">
            <a className="btns btns-success" onClick={_handelSendChanged}>
              {" "}
              ثبت{" "}
            </a>
            <a className="btns btns-warning" onClick={_handelEdit}>
              {" "}
              لغو{" "}
            </a>
          </div>
        ) : (
          ""
        )}
      </div>

      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {state.change
          ? state.StateInfo &&
            state.StateInfo.map((Info, index) => {
              return <input key={index} onChange={e => _handelOnchange(e, index)} value={Info} />;
            })
          : Info &&
            Info.map((Info, index) => {
              return (
                <p key={index}>
                  {Info}
                  {Info.length - 1 > index ? " " : " "}
                </p>
              );
            })}
      </div>
    </div>
  );
};

export default DetailsArrayInput;
