import React, { useState } from "react";
import "./index.scss";
import formatMoney from "../../../util/formatMoney";
const DetailsPrice = ({ Info, Id, refreshComponent, label, fieldName, sendNewVal, toastify }) => {
  const [state, setState] = useState({
    changeDescription: false,
    Info: Info
  });

  const _handelEdit = () => {
    setState(prev => ({
      ...prev,
      changeDescription: !state.changeDescription,
      Info: Info
    }));
  };
  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.Info };
    sendNewVal(data);

    _handelEdit();
  };
  const _handelOnchange = e => {
    setState({ ...state, Info: e.currentTarget.value });
  };

  return (
    <div className="card-details-row">
      <div className="about-title">
        <span>{label} :</span> <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>
        {state.changeDescription ? (
          <div className="btns-container">
            <a className="btns btns-success" onClick={_handelSendChanged}>
              {" "}
              ثبت{" "}
            </a>
            <a className="btns btns-warning" onClick={_handelEdit}>
              {" "}
              لغو{" "}
            </a>
          </div>
        ) : (
          ""
        )}
      </div>

      <div className="fontSize2em InfoColor"> {state.changeDescription ? <input onChange={_handelOnchange} value={state.Info} /> : formatMoney(Info)}</div>
    </div>
  );
};

export default DetailsPrice;
