import React, { useState, Fragment } from "react";
import "./index.scss";
import ModalBox from "../../../util/modals/ModalBox";
import InputsType from "../../UI/Inputs/InputsType";

const DetailsArrayThumbnail = ({ cover, fieldName, sendNewVal, elementType, label, imageType, toastify }) => {
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    index: false,
    files: [...cover],
    change: false
  });

  const stateData = {
    Form: {
      file: {
        label: label,
        elementType: elementType,
        kindOf: "image",
        value: [],
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    },
    formIsValid: false
  };

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false });
    }, 200);
  };
  const onShowlModal = index => {
    if (index) setModalInpts({ ...ModalInpts, show: true, index: index });
    else setModalInpts({ ...ModalInpts, show: true, index: false });
  };
  const returnUpload = async val => {
    let newModalInpts = { ...ModalInpts.files };
    let InfoChange = ModalInpts.files;
    if (ModalInpts.index) {
      newModalInpts[ModalInpts.index] = val;
      InfoChange[ModalInpts.index] = newModalInpts[ModalInpts.index];
    }

    InfoChange.push(val);
    console.log({ newModalInpts, InfoChange });
    setModalInpts({ ...ModalInpts, files: InfoChange });
  };
  const _handelSendChanged = () => {
    const data = { fieldChange: fieldName, newValue: ModalInpts.files };
    sendNewVal(data);

    _handelEdit();
  };
  const _handelEdit = index => {
    setModalInpts(prev => ({
      ...prev,
      change: !ModalInpts.change,
      files: [...cover]
    }));
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <InputsType stateData={stateData} imageType={imageType ? imageType : fieldName} onHideModal={onHideModal} inputsData={returnUpload} showModal={ModalInpts.show} acceptedTitle="ثبت" />
    </ModalBox>
  );
  return (
    <Fragment>
      {renderModalInputs}
      <div className="card-details-row cover-image width100">
        <div className="about-title">
          <span>{"عکس های اسلاید :"}</span>
          {ModalInpts.change ? (
            <div className="btns-container">
              <a className="btns btns-add" onClick={onShowlModal}>
                {" "}
                افزودن{" "}
              </a>
              <a className="btns btns-success" onClick={_handelSendChanged}>
                {" "}
                ثبت{" "}
              </a>
              <a className="btns btns-warning" onClick={_handelEdit}>
                {" "}
                لغو{" "}
              </a>
            </div>
          ) : (
            <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84 positionUnset"></i>
          )}
        </div>
        <div className="centerAll images-wrapper">
          {ModalInpts.change
            ? ModalInpts.files &&
              ModalInpts.files.map((slides, index) => {
                return (
                  <div key={index}>
                    <img src={slides} />
                    <i onClick={() => onShowlModal(index)} className=" icon-pencil transition0-2 rotate-84"></i>
                  </div>
                );
              })
            : cover.map((slides, index) => {
                return (
                  <div key={index}>
                    <img src={slides} />
                  </div>
                );
              })}
        </div>
      </div>
    </Fragment>
  );
};

export default DetailsArrayThumbnail;
