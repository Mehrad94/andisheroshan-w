import { useRef, useState } from "react";

const useScrollDrag = () => {
  const ref = useRef();
  const [initMouseX, setinitMouseX] = useState(0);
  const [initScrollX, setInitScrollX] = useState(0);

  const events = {
    ref: ref,
    // onMouseDown: ({ nativeEvent: e }) => {
    //   console.log({ nativeEventDown: e });
    //   console.log({ scrollLeft: ref.current.scrollLeft });

    //   setinitMouseX(e.clientX);
    //   setInitScrollX(ref.current.scrollLeft);
    // },
    onScroll: ({ nativeEvent: e }) => {
      console.log({ e });
      setinitMouseX(e.clientX);
      setInitScrollX(ref.current.scrollLeft);
      e.preventDefault();
      // ref.current.scrollTo({
      //   left: initScrollX + initMouseX - e.clientX,
      //   behavior: "smooth"
      // });
    }
  };

  return [events, ref];
};
export default useScrollDrag;
