import React, { useState, useEffect } from "react";
import "./index.scss";
import PlayBox from "../../../../../components/PlayBox";
import IsNull from "../../../../../components/IsNull";

const CourseSections = ({ sections, onShowModalInputs, handelEdit, handelRemove }) => {
  const [state, setState] = useState({
    video: [],
    voice: [],
  });
  useEffect(() => {
    let video = [];
    let voice = [];

    sections &&
      sections.map((section, index) => {
        switch (section.type) {
          case "VIDEO":
            video.push(section);
            break;
          case "VOICE":
            voice.push(section);
            break;
          default:
            break;
        }

        setState((prev) => ({ ...prev, video, voice }));
      });
  }, [sections]);
  const elements = (
    <div className="card-details-row">
      <div className="courses-title">
        <span>قسمت ها</span>
        <div className="centerAll">({state.video.length + state.voice.length})</div>

        <div className="add-new-Course">
          <button type="button" className="btn btn-primary" onClick={() => onShowModalInputs({ kindOf: "add" })}>
            افزودن
          </button>{" "}
        </div>
      </div>
      <div className="show-courses-container">
        <div className="show-courses">
          {state.video.length ? (
            <PlayBox playSrc={(event) => onShowModalInputs({ ...event, kindOf: "src" })} handelEdit={handelEdit} data={state.video} name={"VIDEO"} handelRemove={handelRemove} />
          ) : (
            <IsNull title={"خالی می باشد"} />
          )}
        </div>
        <div className="show-courses">
          {state.video.length ? (
            <PlayBox playSrc={(event) => onShowModalInputs({ ...event, kindOf: "src" })} handelEdit={handelEdit} data={state.voice} name={"VOICE"} handelRemove={handelRemove} />
          ) : (
            <IsNull title={"خالی می باشد"} />
          )}{" "}
        </div>
      </div>
    </div>
  );
  return elements;
};

export default CourseSections;
