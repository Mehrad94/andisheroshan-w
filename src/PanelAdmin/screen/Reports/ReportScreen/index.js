import React, { useEffect, useState } from "react";
import { get } from "../../../api";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
import InputDropDownSearch from "../../../components/UI/Inputs/InputDropDownSearch";
import table from "../../../util/consts/table";
import DropdownBoot from "../../../components/UI/Inputs/DropdownBoot";
const ReportScreen = () => {
  const [Reports, setReports] = useState();
  const [Loading, setLoading] = useState(false);
  const [state, setState] = useState({ name: "", data: [], dataHead: [] });
  // console.log({ Reports });
  useEffect(() => {
    handelgetApi();
  }, []);
  const handelgetApi = () => {
    get.reports(setReports, "", setLoading);
  };
  let allSoldSeminar, allSoldAudioBook, allSoldCourse, showData;
  useEffect(() => {
    if (Reports) {
      allSoldSeminar = { data: table.reportsSoldSeminar(Reports.allSoldSeminar)[1], dataHead: table.reportsSoldSeminar(Reports.allSoldSeminar)[0] };
      allSoldAudioBook = { data: Reports.allSoldAudioBook };
      allSoldCourse = { data: Reports.allSoldCourse };
    }
    // console.log({ allSoldSeminar, allSoldAudioBook, allSoldCourse });
  }, [Reports]);
  console.log({ state });
  console.log({ allSoldSeminar, allSoldAudioBook, allSoldCourse });

  const dropDown = [{ title: "کتاب صوتی" }, { title: "دوره" }, { title: "سمینار" }];
  let dropDownData = [];
  for (const index in dropDown) dropDownData.push({ value: dropDown[index].title, title: dropDown[index].title });
  const accepted = (value) => {
    console.log({ value });
    let data;
    switch (value) {
      case "سمینار":
        data = allSoldSeminar;
        break;

      default:
        break;
    }
    setState({ ...state, name: value, ...data });
  };
  const showDataElement = (
    <div className="countainer-main centerAll scale-up-ver-top">
      <div className="elemnts-box  boxShadow">
        <div className="subtitle-elements">
          <span className="centerAll"> {"گزارشات"}</span>
          <div className="btns-container">
            <DropdownBoot dropDownData={dropDownData} accepted={accepted} value={state.name} />
          </div>
        </div>

        {/* <TabelBasic
          subTitle={"دسته بندی ها"}
          btnHead={{ title: "افزودن", onClick: () => onShowlModal({ kindOf: "addData" }) }}
          imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
          tbody={ShowCategory(Categories.docs)[1]}
          thead={ShowCategory(Categories.docs)[0]}
          onClick={_tableOnclick}
        /> */}
      </div>
    </div>
  );
  return Loading ? (
    <div className="staticStyle bgWhite">
      <SpinnerRotate />
    </div>
  ) : (
    showDataElement
  );
};
export default ReportScreen;
