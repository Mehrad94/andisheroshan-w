import React, { useState, useEffect } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
const FormInputSeminar = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, editData, staticTitle } = props;
  const [Admins, setAdmins] = useState([]);
  let loading = (parm) => parm;

  useEffect(() => {
    get.admins(setAdmins, "", loading);
  }, []);
  console.log({ Admins });
  let AdminsData = [];
  for (const index in Admins) {
    if (Admins[index].pubType ? Admins[index].pubType === "Seminar" : true)
      AdminsData.push({ value: Admins[index]._id, title: Admins[index].fullName, description: Admins[index].phoneNumber, image: Admins[index].cover });
  }
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed,
          display,
          dropDownData,
          accepted,
          progress,
          parentType = true,
          disabled = parentType ? false : true;
        if (state.progressPercentPlay || state.progressPercentImage) disabled = true;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "publisher") dropDownData = AdminsData;
        else if (formElement.id === "video") progress = state.progressPercentPlay;
        else if (formElement.id === "cover") progress = state.progressPercentImage;
        changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            dropDownData={AdminsData}
            checkSubmited={checkSubmited}
            disabled={disabled}
            display={display}
            defaultInputDesable={true}
            staticTitle={staticTitle.name === formElement.id ? staticTitle.value : ""}
          />
        );
        return form;
      })}
    </form>
  );
};

export default FormInputSeminar;
