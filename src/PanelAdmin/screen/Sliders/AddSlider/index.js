import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post, put } from "../../../api";
import FormInputSlider from "./FormInputSlider";
import onChanges from "../../../util/onChanges";
import states from "../../../util/consts/states";
const AddSlider = (props) => {
  const { editData, modalShow, onHideModal, sliderId } = props;
  const [data, setData] = useState(states.addSlider);
  const [state, setState] = useState({ progressPercentImage: null, remove: { index: "", name: "" } });
  const [removeStateData, setRemoveStateData] = useState("");
  const [Loading, setLoading] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  useEffect(() => {
    if (!modalShow) setStaticTitle(false);
  }, [modalShow]);

  const [checkSubmited, setCheckSubmited] = useState(false);
  console.log({ removeStateData });

  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async (e) => {
    e.preventDefault();
    let apiRoute;
    const formData = {};
    for (let formElementIdentifier in data.Form) if (formElementIdentifier !== removeStateData) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (editData) apiRoute = put.slider(Object.assign({ _id: sliderId }, { data: formData }));
    else apiRoute = post.slider(formData);
    if (await apiRoute) {
      submited();
      setData(states.addSlider);
      if (modalShow) onHideModal();
    }
    console.log({ formData });
  };

  // ========================= End submited =================
  const inputChangedHandler = async (event, initial, array) => {
    console.log({ event, initial, array });
    let value = event.value;
    // let datas = initial && !editData ? states.addSlider : data;
    let validName;
    if (initial || array) {
      if (event.length > 0) {
        let val = event.filter((d) => {
          if (d.name === "type") return d.value;
        });
        value = val[0].value;
      }
      if (value === "BLOG") validName = "course";
      else validName = "blog";
      if (!array) submited();
    }
    if (validName) setRemoveStateData(validName);
    await onChanges.globalChange({ event, data, setData, state, setState, setLoading, imageType: "sliders", validName });
  };
  useEffect(() => {
    let arrayData = [];
    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) {
          if (stateArray[index].id === key) {
            if (key === "blog" || key === "course") {
              console.log({ key, editData: editData[key] });
              if (editData[key]) setStaticTitle({ value: editData[key] ? editData[key].title : "", name: key });
              // console.log("club discount " + { editData: editData[key] });
              arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
            } else arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
          }
        }
    console.log({ arrayData });

    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputSlider
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
      editData={editData}
      staticTitle={staticTitle}
      checkSubmited={modalShow}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{editData ? "تغییر در اسلایدر " : "افزودن اسلایدر جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddSlider;
