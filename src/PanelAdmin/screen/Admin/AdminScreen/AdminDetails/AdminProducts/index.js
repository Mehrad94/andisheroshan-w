import React, { Fragment } from "react";
import "./index.scss";
import ShowDataCard from "../ShowDataCard";
import card from "../../../../../util/consts/card";

const AdminProducts = ({ soldSeminar, soldCourse, soldAudioBook, products }) => {
  const elements = (
    <Fragment>
      {products && products.data.length > 0 ? <ShowDataCard subTitle={"فایل ها"} length={products.data.length} title={products.title} data={products.card(products.data)} submitedTitle={false} /> : ""}
      {soldCourse && soldCourse.length > 0 ? <ShowDataCard subTitle={"فایل های فروخته شده"} length={soldCourse.length} title={"دوره"} data={card.soldCourse(soldCourse)} submitedTitle={false} /> : ""}
      {soldAudioBook && soldAudioBook.length > 0 ? <ShowDataCard subTitle={"فایل های فروخته شده"} length={soldAudioBook.length} title={"کتاب صوتی"} data={card.soldAudioBook(soldAudioBook)} submitedTitle={false} /> : ""}
      {soldSeminar && soldSeminar.length > 0 ? <ShowDataCard subTitle={"فایل های فروخته شده"} length={soldSeminar.length} title={"سمینار"} data={card.soldSeminar(soldSeminar)} submitedTitle={false} /> : ""}
    </Fragment>
  );
  return elements;
};

export default AdminProducts;
