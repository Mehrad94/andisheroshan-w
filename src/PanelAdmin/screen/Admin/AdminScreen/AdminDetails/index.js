import React, { useState, useEffect, useMemo } from "react";
import { Button, ButtonToolbar } from "react-bootstrap";
import ModalSrc from "../../../../util/modals/ModalSrc";
import ModalBox from "../../../../util/modals/ModalBox";
import { put, post, get } from "../../../../api";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import toastify from "../../../../util/toastify";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsInputSearch from "../../../../components/DetailsComponent/DetailsInputSearch";
import InputsType from "../../../../components/UI/Inputs/InputsType";
import AdminProducts from "./AdminProducts";
import card from "../../../../util/consts/card";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
import dictionary from "../../../../util/dictionary";
const AdminDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  console.log({ information });
  let pubType = information.pubType;
  let cardAdmin;
  switch (pubType) {
    case "Seminar":
      cardAdmin = card.seminar;
      break;
    case "Course":
      cardAdmin = card.course;
      break;
    case "AudioBook":
      cardAdmin = card.audioBook;
      break;
    default:
      break;
  }
  const sendNewVal = async (data) => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  const elements = (
    <React.Fragment>
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        {/* <DetailsThumbnail toastify={toastify} label={"عکس"} elementType={"inputFile"} imageType={"avatar"} fieldName={"avatar"} cover={information.avatar} sendNewVal={sendNewVal} fixed /> */}
        <div className="detailsRow-wrapper">
          <div>
            {" "}
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.fullName} fieldName={"fullName"} label={"نام و نام خانوادگی"} fixed />
          </div>
          <div>
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.phoneNumber} fieldName={"phoneNumber"} label={"شماره همراه"} fixed />

            {/* <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.address} Id={information._id} fieldName={"address"} label={"آدرس "} fixed /> */}
            {/* <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} fixed /> */}
          </div>
        </div>
        <AdminProducts
          soldSeminar={information.soldSeminar}
          soldCourse={information.soldCourse}
          soldAudioBook={information.soldAudioBook}
          products={{ data: information.products, title: dictionary(pubType), card: cardAdmin }}
        />
      </div>
    </React.Fragment>
  );
  return elements;
};

export default AdminDetails;
