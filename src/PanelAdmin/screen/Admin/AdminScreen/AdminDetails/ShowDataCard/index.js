import React from "react";
import ShowCardInformation from "../../../../../components/ShowCardInformation";

const ShowDataCard = (props) => {
  const { subTitle, length, title, data, submitedTitle } = props;
  return (
    <div className="card-details-row">
      <div className="courses-title">
        <span>{subTitle}</span>
        {/* <div className="centerAll">({state.video.length + state.voice.length})</div> */}
      </div>
      <div className="show-courses-container">
        <div className="show-courses">
          <div className="kind-of-data">
            <span>{title}</span> <div className="centerAll"> ({length})</div>
          </div>
          <ShowCardInformation data={data} submitedTitle={submitedTitle} />
        </div>
      </div>
    </div>
  );
};

export default ShowDataCard;
