import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post } from "../../../api";
import FormInputBlog from "./FormInputBlog";
import onChanges from "../../../util/onChanges";
import states from "../../../util/consts/states";
const AddBlog = props => {
  const { editData, modalShow, onHideModal } = props;
  const [data, setData] = useState(states.addBlog);
  const [state, setState] = useState({ progressPercentImage: null, remove: { index: "", name: "" } });
  const [removeStateData, setRemoveStateData] = useState("");
  const [Loading, setLoading] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  useEffect(() => {
    if (!modalShow) setStaticTitle(false);
  }, [modalShow]);

  const [checkSubmited, setCheckSubmited] = useState(false);
  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async e => {
    e.preventDefault();
    let apiRoute;
    const formData = {};
    for (let formElementIdentifier in data.Form) if (formElementIdentifier !== removeStateData) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    apiRoute = post.blog(formData);
    if (await apiRoute) {
      submited();
      setData(states.addBlog);
    }
    console.log(formData);
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event, initial, array) => {
    console.log({ event, initial, array });
    let datas = initial ? states.addBlog : data;
    let value;
    let changeState;
    if (initial) {
      if (event.value === "VOICE") value = "voice";
      else value = "video";
      submited();
      changeState = { name: "dataUrl", argoman: "kindOf", value };
    }
    await onChanges.globalChange({ event, data: datas, setData, state, setState, setLoading, imageType: "sliders", changeState });
  };
  useEffect(() => {
    let arrayData = [];

    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) {
          if (stateArray[index].id === key) {
            if (key === "club" || key === "discount") {
              console.log({ key, editData: editData[key] });
              if (editData[key]) setStaticTitle({ value: editData[key] ? editData[key].title : "", name: key });
              // console.log("club discount " + { editData: editData[key] });
              arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
            } else arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
          }
        }

    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputBlog
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
      editData={editData}
      staticTitle={staticTitle}
    />
  );
  return (
    <div className="countainer-main centerAll formFlex">
      <div className="form-countainer">
        <div className="form-subtitle">{editData ? "تغییر در بلاگ " : "افزودن بلاگ جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddBlog;
