import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const sliders = async (returnData, loading) => {
  return axios
    .get(Strings.ApiString.SLIDER)
    .then((sliders) => {
      console.log({ sliders });
      returnData(sliders.data);
      loading(false);
    })
    .catch((error) => {
      //console.log({ error });

      let data = error.response.data;
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (data)
        if (data.CODE)
          switch (data.CODE) {
            case 1098:
              toastify("شما نمی توانید به این قسمت ورود کنید", "error");
              break;
            default:
              toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
              break;
          }
      return false;
    });
};

export default sliders;
