import courses from "./courses";
import categoreis from "./categoreis";
import blogs from "./blogs";
import sliders from "./sliders";
import searchCourse from "./searchCourse";
import searchBlog from "./searchBlog";
import users from "./users";
import seminars from "./seminars";
import audioBooks from "./audioBooks";
import introductionJob from "./introductionJob";
import admins from "./admins";
import searchPublisher from "./searchPublisher";
import reports from "./reports";

const get = {
  courses,
  categoreis,
  blogs,
  sliders,
  searchCourse,
  searchBlog,
  users,
  seminars,
  audioBooks,
  introductionJob,
  admins,
  searchPublisher,
  reports,
};
export default get;
