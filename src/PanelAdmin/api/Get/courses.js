import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const courses = async (loading, page, returnData) => {
  loading(true);
  return axios
    .get(Strings.ApiString.COURSE + "/" + page)
    .then((courses) => {
      console.log({ courses });
      returnData(courses.data);
      loading(false);
    })
    .catch((error) => {
      //console.log({ error });

      let data = error.response.data;
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (data)
        if (data.CODE)
          switch (data.CODE) {
            case 1098:
              toastify("شما نمی توانید به این قسمت ورود کنید", "error");
              break;
            default:
              toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
              break;
          }
      return false;
    });
};

export default courses;
