import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const adminSearch = async (param, returnData) => {
  loading(true);
  return axios
    .get(Strings.ApiString.ADMIN_SEARCH + "/" + param)
    .then((adminSearch) => {
      console.log({ adminSearch });
      returnData(adminSearch.data);
      loading(false);
    })
    .catch((error) => {
      //console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data)
        switch (error.response.data.Error) {
          default:
            toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
            break;
        }
      return false;
    });
};

export default adminSearch;
