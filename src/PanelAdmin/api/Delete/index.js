import course from "./course";
import blog from "./blog";
import club from "./club";
import slider from "./slider";
import seminar from "./seminar";
import audioBook from "./audioBook";
import introductionJob from "./introductionJob";
import admin from "./admin";
import user from "./user";
import category from "./category";
import audioBookSection from "./audioBookSection";
import courseSection from "./courseSection";
import seminarSection from "./seminarSection";
import introductionJobSection from "./introductionJobSection";

const Delete = { introductionJobSection, seminarSection, courseSection, audioBookSection, category, user, admin, introductionJob, course, blog, club, slider, seminar, audioBook };
export default Delete;
