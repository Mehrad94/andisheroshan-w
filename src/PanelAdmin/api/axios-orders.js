import axios from "axios";
import Cookie from "js-cookie";

const instance = axios.create({ baseURL: "https://andisheroshan.com/api/v1" });
instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("andisheToken");

export default instance;
