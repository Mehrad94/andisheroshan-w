import videoUpload from "./videoUpload";
import voiceUpload from "./voiceUpload";
import imageUpload from "./imageUpload";
import courseSection from "./courseSection";
import audioBookSection from "./audioBookSection";
import course from "./course";
import category from "./category";
import blog from "./blog";
import slider from "./slider";
import login from "./login";
import seminar from "./seminar";
import audioBook from "./audioBook";
import introductionJob from "./introductionJob";
import seminarSection from "./seminarSection";
import introductionJobSection from "./introductionJobSection";
const post = {
  voiceUpload,
  videoUpload,
  imageUpload,
  courseSection,
  audioBookSection,
  course,
  category,
  blog,
  slider,
  login,
  seminar,
  audioBook,
  introductionJob,
  seminarSection,
  introductionJobSection
};
export default post;
