import axios from "../axios-orders";
import Cookies from "js-cookie";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";
import routes from "../../value/routes";

const login = async (param, setLoading) => {
  let URL = Strings.ApiString.LOGIN;
  //console.log({ param });
  // setLoading(true);

  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });

      if (Response.data);
      Cookies.set("andisheToken", Response.data.token, { expires: 7 });
      toastify("شما تایید شده اید", "success");

      setTimeout(() => {
        window.location = routes.GS_PANEL_ADMIN_TITLE;
      }, 1000);

      return true;
    })
    .catch((error) => {
      console.log({ error });
      setLoading(false);

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");
      else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default login;
