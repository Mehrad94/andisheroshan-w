import axios from "../axios-orders";
import axios2 from "axios";

import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const voiceUpload = async (files, setLoading, type, setState, cancelUpload) => {
  setLoading(true);

  // ============================================= const
  const CancelToken = axios2.CancelToken;
  const source = CancelToken.source();

  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      setState(prev => ({ ...prev, progressPercentPlay: percentCompleted }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  const URL = Strings.ApiString.VOICE_UPLOAD;
  formData.append("voice", files);

  // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  //console.log({ formData });
  //console.log({ update });

  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios
  return axios
    .post(URL, formData, settings)
    .then(Response => {
      //console.log({ Response });
      setLoading(false);

      // update[name] = Response.data.voiceUrl;
      return Response.data.voiceUrl;
    })
    .catch(error => {
      //console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default voiceUpload;
