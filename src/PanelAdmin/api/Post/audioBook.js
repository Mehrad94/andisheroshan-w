import axios from "../axios-orders";

import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const audioBook = async (param, setLoading) => {
  let URL = Strings.ApiString.AUDIO_BOOK;
  //console.log({ param });
  // setLoading(true);

  return axios
    .post(URL, param)
    .then(Response => {
      // console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch(error => {
      //console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default audioBook;
