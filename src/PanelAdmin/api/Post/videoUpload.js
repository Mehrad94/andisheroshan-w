import axios from "../axios-orders";
import axios2 from "axios";

import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const videoUpload = async (files, setLoading, type, setState, cancelUpload) => {
  // //console.log({ update });
  setLoading(true);
  // ============================================= const
  const CancelToken = axios2.CancelToken;
  const source = CancelToken.source();

  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      setState(prev => ({ ...prev, progressPercentPlay: percentCompleted }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  const URL = Strings.ApiString.VIDEO_UPLOAD;
  formData.append("video", files);
  // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  //console.log({ formData });
  // ============================================== End log ==============================================

  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return axios
    .post(URL, formData, settings)
    .then(Response => {
      console.log({ Response });
      setLoading(false);
      return Response.data.videoUrl;
      // update[name] = Response.data.videoUrl;
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};
export default videoUpload;
