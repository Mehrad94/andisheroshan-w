import React from "react";
import Header from "./components/Header";
import SideMenu from "./components/SideMenu";
import PanelAdminMain from "./components/PanelAdminMain";
// import "./index.scss";
import checkAuth from "./util/chechkAuth";

const PanelAdmin = () => {
  return (
    <div className="panelAdmin-wrapper">
      {checkAuth() && (
        <React.Fragment>
          <SideMenu />
          <Header />
          <PanelAdminMain />
        </React.Fragment>
      )}
    </div>
  );
};

export default PanelAdmin;
